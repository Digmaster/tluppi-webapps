#Use this script to populate the |restaurant| table of the database

import mysql.connector
import json
from decimal import *
import random

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
cursor = cnx.cursor()

#Load json file
inputFile = open('restaurantData.json','r')
restaurantDict = json.load(inputFile)
inputFile.close()


#Loop through the restaurants and add info and menu to database
for resKey, restaurant in restaurantDict.iteritems():
	
	###############################
	## Add restaurant info first ##
	###############################

	inputDict = {
		'Restaurant_ID' : resKey,
		'name' : restaurant.get('name'),
		'website_url' : restaurant.get('website_url'),
		'facebook_url' : restaurant.get('facebook_url'),
		'redirected_from' : restaurant.get('redirected_from'),
		'twitter_id' : restaurant.get("twitter_id"),
		'has_menu' : restaurant.get('has_menu'),
		'phone' : restaurant.get('phone'),
		'locality' : restaurant.get('locality'),
		'country' : restaurant.get('country'),
		'street_address' : restaurant.get('street_address'),
		'region' : restaurant.get('region'),
		'postal_code' : restaurant.get('postal_code'),
		'lat' : restaurant.get('lat'),
		'long' : restaurant.get('long'),
		'resource_uri' : restaurant.get('resource_uri'),
	}

	#Insert this info into the database
	addRestaurant = ("INSERT INTO `Restaurants` (`Restaurant_ID`, `name`, `website_url`, `facebook_url`, `redirected_from`, `twitter_id`, `has_menu`, `phone`, `locality`, `country`, `street_address`, `region`, `postal_code`, `lat`, `long`, `resource_uri`) VALUES (%(Restaurant_ID)s, %(name)s, %(website_url)s, %(facebook_url)s, %(redirected_from)s, %(twitter_id)s, %(has_menu)s, %(phone)s, %(locality)s, %(country)s, %(street_address)s, %(region)s, %(postal_code)s, %(lat)s, %(long)s, %(resource_uri)s)")
	cursor.execute(addRestaurant,inputDict)

	if 'open_hours' in restaurant.keys():
		for key, hour in restaurant['open_hours'].iteritems():
			if(len(hour)!=0):
				start = hour[0].split(' - ')[0]
				end = hour[0].split(' - ')[1]
				inputDict = {
					'Restaurant_ID' : resKey,
					'Day' : key,
					'Open' : start,
					'Close' : end
				}

				addHour = ("INSERT INTO Open_Hours (Restaurant_ID, Day, Open, Close) VALUES (%(Restaurant_ID)s, %(Day)s, %(Open)s, %(Close)s)")
				cursor.execute(addHour, inputDict)

	if 'menus' in restaurant.keys():
		for menu in restaurant['menus']:
			inputDict = {
				'Restaurant_ID' : resKey,
				'menu_name' : menu.get('menu_name'),
				'currency_symbol' : menu.get('currency_symbol')
			}
			addMenu = ('INSERT INTO Menus (Restaurant_ID, Name, currency_symbol) VALUES (%(Restaurant_ID)s, %(menu_name)s, %(currency_symbol)s)')
			cursor.execute(addMenu, inputDict)
			menuID = cursor.lastrowid

			if 'sections' in menu.keys():
				for section in menu['sections']:
					inputDict = {
						'Menu_ID' : menuID,
						'section_name' : section.get('section_name')
					}

					addSection = ('INSERT INTO Menu_Sections (Menu_ID, section_name) VALUES (%(Menu_ID)s, %(section_name)s)')
					cursor.execute(addSection, inputDict)
					sectionID = cursor.lastrowid

					if 'subsections' in section.keys():
						for sub in section['subsections']:
							inputDict = {
								'Menu_Section_ID' : sectionID,
								'subsection_name' : sub.get('subsection_name')
							}
							addSection = ('INSERT INTO Menu_Subsections (Menu_Section_ID, subsection_name) VALUES (%(Menu_Section_ID)s, %(subsection_name)s)')
							cursor.execute(addSection, inputDict)
							subsectionID = cursor.lastrowid

							if 'contents' in sub.keys():
								for content in sub['contents']:
									inputDict = {
										'Menu_Subsection_ID' : subsectionID,
										'type' : content.get('type'),
										'text' : content.get('text'),
										'description' : content.get('description'),
										'name' : content.get('name'),
										'price' : content.get('price')
									}
									addSubsection = ('INSERT INTO Menu_Contents (Menu_Subsection_ID, type, text, description, name, price) VALUES (%(Menu_Subsection_ID)s, %(type)s, %(text)s, %(description)s, %(name)s, %(price)s)')
									cursor.execute(addSubsection, inputDict)

	if 'similar_venues' in restaurant.keys():
		for similar in restaurant['similar_venues']:
			inputDict = {
				'Restaurant_ID' : resKey,
				'Similar_Restaurant_ID' : similar
			}
			addSimilar = ('INSERT INTO Similar_Venues (Restaurant_ID, Similar_Restaurant_ID) VALUES (%(Restaurant_ID)s, %(Similar_Restaurant_ID)s)')
			cursor.execute(addSimilar, inputDict)

	if 'categories' in restaurant.keys():
		for category in restaurant['categories']:
			inputDict = {
				'Restaurant_ID' : resKey,
				'text' : category
			}
			addCategory = ('INSERT INTO Categories (Restaurant_ID, text) VALUES (%(Restaurant_ID)s, %(text)s)')
			cursor.execute(addCategory, inputDict)

cnx.commit()
cnx.close()
