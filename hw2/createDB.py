#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET latin1") % (DATABASE_NAME))
cursor.execute(createDB)

#########################
## Switch to feednd DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################

#OrderItems
dropTableQuery = ("DROP TABLE IF EXISTS Order_Items")
cursor.execute(dropTableQuery)

#Orders
dropTableQuery = ("DROP TABLE IF EXISTS Orders")
cursor.execute(dropTableQuery)

#Users
dropTableQuery = ("DROP TABLE IF EXISTS Users")
cursor.execute(dropTableQuery)

#Hours
dropTableQuery = ("DROP TABLE IF EXISTS Open_Hours")
cursor.execute(dropTableQuery)

#Categories
dropTableQuery = ("DROP TABLE IF EXISTS Categories")
cursor.execute(dropTableQuery)

#Similar Venues
dropTableQuery = ("DROP TABLE IF EXISTS Similar_Venues")
cursor.execute(dropTableQuery)

#Menu Contents
dropTableQuery = ("DROP TABLE IF EXISTS Menu_Contents")
cursor.execute(dropTableQuery)

#Menu Subsections
dropTableQuery = ("DROP TABLE IF EXISTS Menu_Subsections")
cursor.execute(dropTableQuery)

#Menu Sections
dropTableQuery = ("DROP TABLE IF EXISTS Menu_Sections")
cursor.execute(dropTableQuery)

#Menus
dropTableQuery = ("DROP TABLE IF EXISTS Menus")
cursor.execute(dropTableQuery)

#Restaurant
dropTableQuery = ("DROP TABLE IF EXISTS Restaurants")
cursor.execute(dropTableQuery)


########################
## Create tables next ##
########################


#Restaurant
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Restaurants` (
						`Restaurant_ID` CHAR(20) NOT NULL,
						`name` VARCHAR(45) NULL,
						`locality` VARCHAR(45) NULL,
						`website_url` VARCHAR(45) NULL,
						`facebook_url` VARCHAR(45) NULL,
						`redirected_from` VARCHAR(45) NULL,
						`long` DOUBLE NULL,
						`twitter_id` VARCHAR(45) NULL,
						`has_menu` TINYINT(1) NULL,
						`country` VARCHAR(45) NULL,
						`lat` DOUBLE NULL,
						`postal_code` CHAR(5) NULL,
						`resource_uri` VARCHAR(100) NULL,
						`region` VARCHAR(8) NULL,
						`street_address` VARCHAR(45) NULL,
						`phone` VARCHAR(16) NULL,
						PRIMARY KEY (`Restaurant_ID`))
						'''
                    )
cursor.execute(createTableQuery)

#Open Hours
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Open_Hours` (
						`Restaurant_ID` CHAR(20) NOT NULL,
						`Day` ENUM('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') NOT NULL,
						`Open` TIME NOT NULL,
						`Close` TIME NOT NULL,
						PRIMARY KEY (`Restaurant_ID`, `Day`),
						CONSTRAINT `Restaurant_ID_hour`
						FOREIGN KEY (`Restaurant_ID`)
						REFERENCES `Restaurants` (`Restaurant_ID`)
						ON DELETE CASCADE)
						'''
                    )
cursor.execute(createTableQuery)

#Categories
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Categories` (
						`Restaurant_ID` CHAR(20) NOT NULL,
						`text` VARCHAR(45) NOT NULL,
						PRIMARY KEY (`Restaurant_ID`, `text`),
						CONSTRAINT `Restaurant_ID_cat`
						FOREIGN KEY (`Restaurant_ID`)
						REFERENCES `Restaurants` (`Restaurant_ID`)
						ON DELETE CASCADE)
						'''
                    )
cursor.execute(createTableQuery)

#Similar Venues
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Similar_Venues` (
						`Restaurant_ID` CHAR(20) NOT NULL,
						`Similar_Restaurant_ID` CHAR(20) NOT NULL,
						PRIMARY KEY (`Restaurant_ID`, `Similar_Restaurant_ID`),
						CONSTRAINT `Restaurant_ID_ven1`
						 FOREIGN KEY (`Restaurant_ID`)
						 REFERENCES `Restaurants` (`Restaurant_ID`)
						 ON DELETE CASCADE)
						'''
                    )
cursor.execute(createTableQuery)

#Menus
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Menus` (
						`Menu_ID` INT NOT NULL AUTO_INCREMENT,
						`Restaurant_ID` CHAR(20) NOT NULL,
						`Name` VARCHAR(45) NULL,
						`currency_symbol` CHAR NULL,
						PRIMARY KEY (`Menu_ID`),
						INDEX `Restaurant_ID_idx` (`Restaurant_ID` ASC),
						CONSTRAINT `Restaurant_ID_menu`
						FOREIGN KEY (`Restaurant_ID`)
						REFERENCES `Restaurants` (`Restaurant_ID`)
						ON DELETE CASCADE)
						'''
                    )
cursor.execute(createTableQuery)

#Menu Sections
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Menu_Sections` (
						`Menu_Section_ID` INT NOT NULL AUTO_INCREMENT,
						`Menu_ID` INT NULL,
						`section_name` VARCHAR(45) NULL,
						PRIMARY KEY (`Menu_Section_ID`),
						INDEX `Menu_ID_idx` (`Menu_ID` ASC),
						CONSTRAINT `Menu_ID_sec`
						FOREIGN KEY (`Menu_ID`)
						REFERENCES `Menus` (`Menu_ID`)
						ON DELETE CASCADE)
						'''
                    )
cursor.execute(createTableQuery)

#Menu Subsections
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Menu_Subsections` (
						`Menu_Subsection_ID` INT NOT NULL AUTO_INCREMENT,
						`Menu_Section_ID` INT NULL,
						`subsection_name` VARCHAR(45) NULL,
						PRIMARY KEY (`Menu_Subsection_ID`),
						INDEX `Menu_Section_ID_idx` (`Menu_Section_ID` ASC),
						CONSTRAINT `Menu_Section_ID_subsec`
						FOREIGN KEY (`Menu_Section_ID`)
						REFERENCES `Menu_Sections` (`Menu_Section_ID`)
						ON DELETE CASCADE)
						'''
                    )
cursor.execute(createTableQuery)

#Menu Contents
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Menu_Contents` (
						`Menu_Content_ID` INT NOT NULL AUTO_INCREMENT,
						`Menu_Subsection_ID` INT NOT NULL,
						`text` VARCHAR(200) NULL,
						`type` VARCHAR(45) NOT NULL,
						`name` VARCHAR(45) NULL,
						`description` VARCHAR(45) NULL,
						`price` VARCHAR(45) NULL,
						PRIMARY KEY (`Menu_Content_ID`),
						INDEX `Menu_Subsection_ID_idx` (`Menu_Subsection_ID` ASC),
						CONSTRAINT `Menu_Subsection_ID_cont`
						FOREIGN KEY (`Menu_Subsection_ID`)
						REFERENCES `Menu_Subsections` (`Menu_Subsection_ID`)
						ON DELETE CASCADE)
						'''
                    )
cursor.execute(createTableQuery)

#Orders 
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Orders` (
						  `Order_ID` INT NOT NULL AUTO_INCREMENT,
						  `User_ID` VARCHAR(45) NULL,
						  `Date_Created` DATETIME NULL,
						  `Status` INT NOT NULL DEFAULT 0, # 0: in progress 1: completed
						  PRIMARY KEY (`Order_ID`))
						'''
                    )
cursor.execute(createTableQuery)

#Order Item
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Order_Items` (
						  `Order_ID` INT NOT NULL,
						  `Menu_Content_ID` INT NOT NULL,
						  `Quantity` INT UNSIGNED NULL DEFAULT 1,
						  PRIMARY KEY (`Order_ID`, `Menu_Content_ID`))
						'''
                    )
cursor.execute(createTableQuery)

#User
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Users` (
						  `User_ID` INT NOT NULL AUTO_INCREMENT,
						  `Phone_Number` VARCHAR(45) NOT NULL,
						  PRIMARY KEY (`User_ID`))
						'''
                    )
cursor.execute(createTableQuery)


#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()