import cherrypy
import sys
import mysql.connector
from collections import OrderedDict
import mysql.connector
import json
from decimal import *
import random

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'

class ExampleApp(object):
	def __init__(self):
		self.restaurants = Restaurant()

	@cherrypy.expose
	def index(self):
		cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME, use_unicode=true)
		cursor = cnx.cursor()

		query = ("SELECT Restaurant_ID, name, locality FROM Restaurants")
		cursor.execute(query)


		rtn = """<html>
						<head>
							<style>
							table, th, td {
								border: 1px solid black;
							}
							</style>
							<title>Tom's Resturants</title>
						</head>
						<body>
							<table width="100%">
								<tr>
									<th>Orders</th>	<th>Resturants</th>	<th>Account</th>
								</tr>
							</table>
							<br><br>
							<table>
								<tr>
									<th>Resurant</th><th>Location</th><th>Link</th>
								</tr>"""
		for (ID, name, locality) in cursor:
			rtn = rtn+"<tr><td>"+name+"</td><td>"+locality+"</td><td><a href=\"restaurants/"+ID+"\">Clicky</a></td></tr>"
		rtn = rtn+"</table></body></html>"
		return rtn
	@cherrypy.expose
	def showdb(self):
		cnx = mysql.connector.connect(user='test', password='mypass',
							  host='127.0.0.1',
							  database='testdb')
		cursor = cnx.cursor()
		query = ("SELECT firstname,lastname,email FROM Invitations")
		cursor.execute(query)
		info = str()
		print cursor
		for (firstname, lastname, email) in cursor:
		   info = info + "Full Name:" + lastname + firstname + "Email: "+email
		return info

@cherrypy.popargs('ID')
class Restaurant(object):
	@cherrypy.expose
	def index(self, ID):
		cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
		cursor = cnx.cursor()

		query = ("SELECT m.Name, section_name, subsection_name, mc.name, description, currency_symbol, price FROM Restaurants r JOIN Menus m ON r.Restaurant_ID = m.Restaurant_ID JOIN Menu_Sections ms ON ms.Menu_ID = m.Menu_ID JOIN Menu_Subsections mss ON mss.Menu_Section_ID = ms.Menu_Section_ID JOIN Menu_Contents mc ON mc.Menu_Subsection_ID = mss.Menu_Subsection_ID WHERE r.Restaurant_ID = '"+ID+"' AND mc.type = 'ITEM'")
		cursor.execute(query)
		rtn = """<html>
				<head>
					<style>
					table, th, td {
						border: 1px solid black;
					}
					</style>
					<title>Tom's Resturants</title>
				</head>
				<body>
					<table width="100%">
						<tr>
							<th>Orders</th>	<th><a href=\"/api/">Resturants</a></th>	<th>Account</th>
						</tr>
					</table>
					<br><br>
					<table>
						<tr>
							<th>Menu</th><th>Section</th><th>Subsection</th><th>Name</th><th>Description</th><th>Price</th>
						</tr>"""
		for (Menu, Section, Subsection, Name, Description, Currency, Price) in cursor:
			rtn = rtn + "<tr><td>"+xstr(Menu)+"</td><td>"+xstr(Section)+"</td><td>"+xstr(Subsection)+"</td><td>"+xstr(Name)+"</td><td>"+xstr(Description)+"</td><td>"+xstr(Currency)+xstr(Price)+"</td></tr>"
		rtn = rtn+"</table></body></html>"
		return rtn

if __name__ == '__main__':
	cherrypy.quickstart(ExampleApp())

application = cherrypy.Application(ExampleApp(), None)

def xstr(s):
    if s is None:
        return ''
    return s