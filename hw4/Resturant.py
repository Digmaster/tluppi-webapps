import cherrypy
import sys
from collections import OrderedDict
import mysql.connector
import json
from decimal import *
import random
import os
import pprint
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'

def xstr(s):
	if s is None:
		return ''
	return s

class Menu():

	@cherrypy.expose
	def index(self, ID, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Menus.Menu_ID, Name, Count(*) FROM Menus LEFT JOIN Menu_Sections ON Menus.Menu_ID = Menu_Sections.Menu_ID WHERE Restaurant_ID = %(Restaurant_ID)s GROUP BY Menu_ID")
			cursor.execute(query, { 'Restaurant_ID': ID })
			result = dict()
			result["menus"] = dict()
			for(ID, name, num) in cursor:
				result["menus"][ID] = dict()
				result["menus"][ID]["name"] = name
				result["menus"][ID]["numSections"] = num

			#if outputformat == 'html':
			#	return env.get_template('restaurants-tmpl.html').render(info=result)
			#elif outputformat == 'json':
			#return out
			return json.dumps(result, encoding='utf-8')

	@cherrypy.expose
	def default(self, ID, menuID, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Menus.Menu_ID, Name, Count(*) FROM Menus LEFT JOIN Menu_Sections ON Menus.Menu_ID = Menu_Sections.Menu_ID WHERE Menus.Menu_ID = %(Menu_ID)s GROUP BY Menu_ID")
			cursor.execute(query, { 'Menu_ID': menuID })
			result = dict()
			for(ID, name, num) in cursor: #I honestly don't know ho wto get data out of a curser outside of doing this
				result["name"] = name
				result["numSections"] = num

			if(len(result)==0):
				result["error"] = "No menu with given ID"
				return json.dumps(result, encoding='utf-8')

			#if outputformat == 'html':
			#	return env.get_template('restaurants-tmpl.html').render(info=result)
			#elif outputformat == 'json':
			return json.dumps(result, encoding='utf-8')

class Hour():

	@cherrypy.expose
	def index(self, ID, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Day, Open, Close FROM Open_Hours WHERE Restaurant_ID = %(Restaurant_ID)s")
			cursor.execute(query, { 'Restaurant_ID': ID })
			result = dict()
			result["hours"] = dict()
			i = 0
			for(day, o, c) in cursor:
				result["hours"][i] = dict()
				result["hours"][i]["day"] = str(day)
				result["hours"][i]["open"] = str(o)
				result["hours"][i]["close"] = str(c)
				i = i + 1

			#if outputformat == 'html':
			#	return env.get_template('restaurants-tmpl.html').render(info=result)
			#elif outputformat == 'json':
			#return out
			return json.dumps(result, encoding='utf-8')

class Section():

	@cherrypy.expose
	def index(self, menuID, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Menu_Sections.Menu_Section_ID, section_name, Count(*) FROM Menu_Sections LEFT JOIN Menu_Subsections ON Menu_Sections.Menu_Section_ID = Menu_Subsections.Menu_Section_ID LEFT JOIN Menu_Contents ON Menu_Subsections.Menu_Subsection_ID = Menu_Contents.Menu_Subsection_ID WHERE Menu_ID = %(Menu_ID)s GROUP BY Menu_Section_ID")
			cursor.execute(query, { 'Menu_ID': menuID })
			result = dict()
			result["sections"] = dict()
			for(ID, name, num) in cursor:
				i = dict()
				i["name"] = name
				i["numItems"] = num
				result["sections"][ID] = i

			#if outputformat == 'html':
			#	return env.get_template('restaurants-tmpl.html').render(info=result)
			#elif outputformat == 'json':
			return json.dumps(result, encoding='utf-8')

	@cherrypy.expose
	def default(self, menuID, sectionID, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Menu_Sections.Menu_Section_ID, section_name, Count(*) FROM Menu_Sections LEFT JOIN Menu_Subsections ON Menu_Sections.Menu_Section_ID = Menu_Subsections.Menu_Section_ID LEFT JOIN Menu_Contents ON Menu_Subsections.Menu_Subsection_ID = Menu_Contents.Menu_Subsection_ID WHERE Menu_Sections.Menu_Section_ID = %(Menu_Section_ID)s GROUP BY Menu_Section_ID")
			cursor.execute(query, { 'Menu_Section_ID': sectionID })
			result = dict()
			for(ID, name, num) in cursor:
				result["name"] = name
				result["numItems"] = num

			if(len(result)==0):
				result["error"] = "No section with given ID"
				return json.dumps(result, encoding='utf-8')

			#if outputformat == 'html':
			#	return env.get_template('restaurants-tmpl.html').render(info=result)
			#elif outputformat == 'json':
			return json.dumps(result, encoding='utf-8')

class Item():
	@cherrypy.expose
	def index(self, sectionID, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Menu_Content_ID, subsection_name, name, description, price FROM Menu_Contents LEFT JOIN Menu_Subsections ON Menu_Contents.Menu_Subsection_ID = Menu_Subsections.Menu_Subsection_ID WHERE Menu_Subsections.Menu_Section_ID = %(Section_ID)s ORDER BY subsection_name, name")
			cursor.execute(query, { 'Section_ID': sectionID })
			result = dict()
			result["items"] = dict()
			for(ID, subSecName, name, desc, price) in cursor:
				i = dict()
				i["subsectionName"] = subSecName
				i["name"] = name
				i["description"] = desc
				i["price"] = price
				result["items"][ID] = i

			#if outputformat == 'html':
			#	return env.get_template('restaurants-tmpl.html').render(info=result)
			#elif outputformat == 'json':
			return json.dumps(result, encoding='utf-8')

	@cherrypy.expose
	def default(self, sectionID, itemID, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Menu_Content_ID, subsection_name, name, description, price FROM Menu_Contents LEFT JOIN Menu_Subsections ON Menu_Contents.Menu_Subsection_ID = Menu_Subsections.Menu_Subsection_ID WHERE Menu_Content_ID = %(Item_ID)s ORDER BY subsection_name, name")
			cursor.execute(query, { 'Item_ID': itemID })
			result = dict()
			for(ID, subSecName, name, desc, price) in cursor:
				result["subsectionName"] = subSecName
				result["name"] = name
				result["description"] = desc
				result["price"] = price

				if(len(result)==0):
					result["error"] = "No item with given ID"
					return json.dumps(result, encoding='utf-8')

			#if outputformat == 'html':
			#	return env.get_template('restaurants-tmpl.html').render(info=result)
			#elif outputformat == 'json':
			return json.dumps(result, encoding='utf-8')

class Restaurant():
	menu = Menu()
	sec = Section()
	item = Item()
	hour = Hour()

	@cherrypy.expose
	def index(self, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Restaurant_ID, name, locality, website_url, facebook_url, redirected_from, twitter_id, has_menu, country, lat, `long`, postal_code, resource_uri, region, street_address, phone FROM Restaurants")
			cursor.execute(query)
			result = dict()
			for(ID, name, locality, website_url, facebook_url, redirected_from, twitter_id, has_menu, country, lat, lon, postal_code, resource_uri, region, street_address, phone) in cursor:
				i = dict()
				i["name"] = name
				i["locality"] = locality
				i['website_url'] = website_url
				i['facebook_url'] = facebook_url
				i['redirected_from'] = redirected_from
				i['twitter_id'] = twitter_id
				i['has_menu'] = has_menu
				i['country'] = country
				i['lat'] = lat
				i['lon'] = lon
				i['postal_code'] = postal_code
				i['resource_uri'] = resource_uri
				i['region'] = region
				i['street_address'] = street_address
				i['phone'] = phone
				result[ID] = i

			if outputformat == 'html':
				return env.get_template('restaurants-tmpl.html').render(info=result)
			elif outputformat == 'json':
				return json.dumps(result, encoding='utf-8')

	@cherrypy.expose
	def default(self, ID, menu="", menuID="", sections="", sectionID="", items="", itemID="", outputformat='html'):
		if(menu==""):
			if(cherrypy.request.method=="GET"):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("SELECT Restaurant_ID, name, locality, website_url, facebook_url, redirected_from, twitter_id, has_menu, country, lat, `long`, postal_code, resource_uri, region, street_address, phone FROM Restaurants WHERE Restaurant_ID = %(Restaurant_ID)s")
				cursor.execute(query, { 'Restaurant_ID': ID })
				i = dict()
				for(ID, name, locality, website_url, facebook_url, redirected_from, twitter_id, has_menu, country, lat, lon, postal_code, resource_uri, region, street_address, phone) in cursor:
					i["name"] = name
					i["locality"] = locality
					i['website_url'] = website_url
					i['facebook_url'] = facebook_url
					i['redirected_from'] = redirected_from
					i['twitter_id'] = twitter_id
					i['has_menu'] = has_menu
					i['country'] = country
					i['lat'] = lat
					i['lon'] = lon
					i['postal_code'] = postal_code
					i['resource_uri'] = resource_uri
					i['region'] = region
					i['street_address'] = street_address
					i['phone'] = phone

				if(len(i)==0):
					i["error"] = "No restaurant with given ID"
					return json.dumps(i, encoding='utf-8')

				if outputformat == 'html':
					return env.get_template('restaurant-tmpl.html').render(info=i)
				elif outputformat == 'json':
					return json.dumps(i, encoding='utf-8')
		elif(menuID==""):
			if(menu=="menu"):
				return self.menu.index(ID, outputformat)
			elif(menu=="hours"):
				return self.hour.index(ID, outputformat)
		elif(sections==""):
			return self.menu.default(ID, menuID, outputformat)
		elif(sectionID==""):
			return self.sec.index(menuID, outputformat)
		elif(items==""):
			return self.sec.default(menuID, sectionID, outputformat)
		elif(itemID==""):
			return self.item.index(sectionID, outputformat)
		else:
			return self.item.default(sectionID, itemID, outputformat)