import cherrypy
import sys
import mysql.connector
from collections import OrderedDict
import mysql.connector
import json
from decimal import *
import random
import os
import re
from passlib.hash import sha256_crypt
from pyvalidate import validate, ValidationException
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'

def xstr(s):
	if s is None:
		return ''
	return s

class User(object):
	@cherrypy.expose
	def default(self, ID, orders="", phone="", outputformat = 'html'):
		if(orders==""):
			if(cherrypy.request.method=="GET"):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("SELECT User_ID, Phone_Number FROM Users WHERE User_ID = %(ID)s")
				cursor.execute(query, { "ID": ID })
				result = dict()
				for(ID, phone) in cursor:
					result['phone'] = phone

				if outputformat == 'html':
					return env.get_template('user-tmpl.html').render(info=result)
				elif outputformat == 'json':
					return json.dumps(result, encoding='utf-8')
			elif(cherrypy.request.method=="POST"):
				if(phone!=""):
					cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
					cursor = cnx.cursor()

					query = ("UPDATE Users Phone_Number=%(num)s WHERE User_ID = %(ID)s;")
					cursor.execute(query, { "num": phone, "ID": ID })
					cnx.commit()
					cnx.close()
					return json.dumps({ "success": True }, encoding='utf-8')
				else:
					cherrypy.reponse.headers["Status"] = "400"
					result["error"] = "No phone number sent"
					return json.dumps(result, encoding='utf-8')
			elif(cherrypy.request.method=='DELETE'):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("DELETE FROM Users WHERE User_ID = %(ID)s;")
				cursor.execute(query, { "ID": ID })
				cnx.commit()
				cnx.close()
				return json.dumps({ "success": True }, encoding='utf-8')
		else:
			if(cherrypy.request.method=="GET"):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("SELECT Order_ID, Date_Created, Status FROM Orders WHERE User_ID = %(ID)s")
				cursor.execute(query, { "ID": ID })
				result = dict()
				for(ID, date, status) in cursor:
					i = dict()
					i['dateCreated'] = date
					i['status'] = status
					result[ID] = date

				if outputformat == 'html':
					return env.get_template('orders-tmpl.html').render(info=result)
				elif outputformat == 'json':
					return json.dumps(result, encoding='utf-8')

	@cherrypy.expose
	def index(self, phone="", outputformat = 'html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT User_ID, Phone_Number, Name, Email FROM Users")
			cursor.execute(query)
			result = dict()
			for(ID, phone, name, email) in cursor:
				i = dict()
				i['phone'] = phone
				i['name'] = name
				i['email'] = email
				result[ID] = i

			if outputformat == 'html':
				return env.get_template('users-tmpl.html').render(info=result)
			elif outputformat == 'json':
				return json.dumps(result, encoding='utf-8')
		elif(cherrypy.request.method=="PUT"):
			if(phone!=""):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("INSERT INTO Users (Phone_Number) VALUES (%(num)s);")
				cursor.execute(query, { "num": phone })
				cnx.commit()
				cnx.close()
				return json.dumps({ "success": True }, encoding='utf-8')
			else:
				cherrypy.reponse.headers["Status"] = "400"
				result["error"] = "No phone number sent"
				return json.dumps(result, encoding='utf-8')

	@validate(requires=['name', 'email', 'password', 'phone'], types={'name':str, 'email':str, 'password':str, 'phone':str}, values={'email':'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', 'phone':'(^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$)|(^\d\d\d-\d\d\d-\d\d\d\d$)'})
	def check_params(self, name, email, password, phone):
			  print 'adding user "%s:%s" with email: %s:%s phone: %s:%s password: %s:%s' % (name, type(name),
						 email, type(email),
						 phone, type(phone),
						 password, type(password))

	@cherrypy.expose
	def create(self, email=None, phone=None, password=None, name=None, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			return env.get_template('users-create-tmpl.html').render()
		else:
			''' Add a new user '''
			result = dict()
			if not name:
			  try:
				name = cherrypy.request.json["name"]
				print "name received: %s" % name
			  except:
				print "name was not received"
				result["error"] = "No name sent"
				return json.dumps(result, encoding='utf-8')
			if not email:
			  try:
				email = cherrypy.request.json["email"]
				print "email received: %s" % email
			  except:
				print "email was not received"
				result["error"] = "Expected email in JSON input"
				return json.dumps(result, encoding='utf-8')
			if not password:
			  try:
				password = cherrypy.request.json["password"]
				print "password received: %s" % password
			  except:
				print "password was not received"
				result["error"] = "Expected password in JSON input"
				return json.dumps(result, encoding='utf-8')
			if not phone:
			  try:
				phone = cherrypy.request.json["phone"]
				print "phone received: %s" % phone
			  except:
				print "phone was not received"
				result["error"] = "Expected phone in input"
				return json.dumps(result, encoding='utf-8')

			try:
				self.check_params(name=name, email=email, password=password, phone=phone)
			except ValidationException as ex:
				print ex.message
				result["error"] = ex.message
				return json.dumps(result, encoding='utf-8')

			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			hashedPass = sha256_crypt.encrypt(password);

			# Check if email already exists
			q="SELECT EXISTS(SELECT 1 FROM Users WHERE Email='%s')" % email
			cursor.execute(q)
			if cursor.fetchall()[0][0]:
				#email already exists
				print "User with email %s Already Exists" % email
				result["error"] = "User with this email already exists"
				return json.dumps(result, encoding='utf-8')

			query = ("INSERT INTO Users (Email, Phone_Number, Password) VALUES (%(email)s, %(num)s, %(password)s);")
			cursor.execute(query, { "email": email, "num": phone, "password": hashedPass })
			cnx.commit()
			cnx.close()
			if outputformat=='html':
				return """<html>
						<head>
							<meta http-equiv="refresh" content="0; url=/restaurants" />
						</head>
						</html>"""
			else:
				return json.dumps({ "success": True }, encoding='utf-8')