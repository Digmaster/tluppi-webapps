import cherrypy
import sys
import mysql.connector
from collections import OrderedDict
import mysql.connector
import json
from decimal import *
import random
import os
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'

def xstr(s):
	if s is None:
		return ''
	return s

class User(object):
	@cherrypy.expose
	def default(self, ID, orders="", phone="", outputformat = 'html'):
		if(orders==""):
			if(cherrypy.request.method=="GET"):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("SELECT User_ID, Phone_Number FROM Users WHERE User_ID = %(ID)s")
				cursor.execute(query, { "ID": ID })
				result = dict()
				for(ID, phone) in cursor:
					result['phone'] = phone

				if outputformat == 'html':
					return env.get_template('user-tmpl.html').render(info=result)
				elif outputformat == 'json':
					return json.dumps(result, encoding='utf-8')
			elif(cherrypy.request.method=="POST"):
				if(phone!=""):
					cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
					cursor = cnx.cursor()

					query = ("UPDATE Users Phone_Number=%(num)s WHERE User_ID = %(ID)s;")
					cursor.execute(query, { "num": phone, "ID": ID })
					cnx.commit()
					cnx.close()
					return json.dumps({ "success": True }, encoding='utf-8')
				else:
					cherrypy.reponse.headers["Status"] = "400"
					result["error"] = "No phone number sent"
					return json.dumps(result, encoding='utf-8')
			elif(cherrypy.request.method=='DELETE'):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("DELETE FROM Users WHERE User_ID = %(ID)s;")
				cursor.execute(query, { "ID": ID })
				cnx.commit()
				cnx.close()
				return json.dumps({ "success": True }, encoding='utf-8')
		else:
			if(cherrypy.request.method=="GET"):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("SELECT Order_ID, Date_Created, Status FROM Orders WHERE User_ID = %(ID)s")
				cursor.execute(query, { "ID": ID })
				result = dict()
				for(ID, date, status) in cursor:
					i = dict()
					i['dateCreated'] = date
					i['status'] = status
					result[ID] = date

				if outputformat == 'html':
					return env.get_template('orders-tmpl.html').render(info=result)
				elif outputformat == 'json':
					return json.dumps(result, encoding='utf-8')

	@cherrypy.expose
	def index(self, phone="", outputformat = 'html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT User_ID, Phone_Number FROM Users")
			cursor.execute(query)
			result = dict()
			for(ID, phone) in cursor:
				i = dict()
				i['phone'] = phone
				result[ID] = i

			if outputformat == 'html':
				return env.get_template('users-tmpl.html').render(info=result)
			elif outputformat == 'json':
				return json.dumps(result, encoding='utf-8')
		elif(cherrypy.request.method=="PUT"):
			if(phone!=""):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("INSERT INTO Users (Phone_Number) VALUES (%(num)s);")
				cursor.execute(query, { "num": phone })
				cnx.commit()
				cnx.close()
				return json.dumps({ "success": True }, encoding='utf-8')
			else:
				cherrypy.reponse.headers["Status"] = "400"
				result["error"] = "No phone number sent"
				return json.dumps(result, encoding='utf-8')