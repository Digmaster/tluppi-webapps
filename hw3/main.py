import cherrypy
import sys
sys.path.append('/var/www/wsgi/')
import mysql.connector
from Resturant import Restaurant
from User import User
from Order import Order
from collections import OrderedDict

class ExampleApp(object):

	@cherrypy.expose
	def index(self):
		rtn = str()
		d = {'Subway': 427, "O'Rourke's Public House":632, 'The Mark': 730}
		dic = OrderedDict(sorted(d.items(), key=lambda t: t[1]))
		rtn = """<html>
						<head>
							<meta http-equiv="refresh" content="0; url=/restaurants" />
							<style>
							table, th, td {
								border: 1px solid black;
							}
							</style>
							<title>Tom's Resturants</title>
						</head>
						<body>
							<table width="100%">
								<tr>
									<th>Orders</th>	<th>Resturants</th>	<th>Account</th>
								</tr>
							</table>
							<br><br>
							<table>
								<tr>
									<th>Resurant</th><th>Distance</th>
								</tr>"""
		for (name, distance) in dic.iteritems():
			rtn = rtn+"<tr><td>"+name+"</td><td>"+str(distance)+"</td></tr>"
		rtn = rtn+"</table></body></html>"
		return rtn
	@cherrypy.expose
	def showdb(self):
		cnx = mysql.connector.connect(user='test', password='mypass',
							  host='127.0.0.1',
							  database='testdb')
		cursor = cnx.cursor()
		query = ("SELECT firstname,lastname,email FROM Invitations")
		cursor.execute(query)
		info = str()
		print cursor
		for (firstname, lastname, email) in cursor:
		   info = info + "Full Name:" + lastname + firstname + "Email: "+email
		return info

root = ExampleApp()
root.restaurants = Restaurant()
root.users = User()
root.orders = Order()

application = cherrypy.Application(root, None)
