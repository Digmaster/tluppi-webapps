import cherrypy
import sys
import mysql.connector
from collections import OrderedDict
import mysql.connector
import json
from decimal import *
import random
import pprint
import os
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'
USER = 2

def xstr(s):
	if s is None:
		return ''
	return s

class Item():
	def index(self, order, outputformat='html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Order_Items.Menu_Content_ID, subsection_name, name, description, price, Quantity FROM Menu_Contents LEFT JOIN Menu_Subsections ON Menu_Contents.Menu_Subsection_ID = Menu_Subsections.Menu_Subsection_ID LEFT JOIN Order_Items ON Order_Items.Menu_Content_ID = Menu_Contents.Menu_Content_ID WHERE Order_ID = %(order)s ORDER BY subsection_name, name")
			cursor.execute(query, { "order":order })
			result = dict()
			result["ID"] = order
			result["items"] = dict()
			for(ID, subSecName, name, desc, price, quantity) in cursor:
				i = dict()
				i["subsectionName"] = subSecName
				i["name"] = name
				i["description"] = desc
				i["price"] = price
				i['quantity'] = quantity
				result["items"][ID] = i

			if outputformat == 'html':
				return env.get_template('items-tmpl.html').render(info=result)
			elif outputformat == 'json':
				return json.dumps(result, encoding='utf-8')

class Order(object):
	item = Item()
	@cherrypy.expose
	def default(self, ID, items="", outputformat = 'html'):
		if(items==""):
			if(cherrypy.request.method=="GET"):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("SELECT Order_ID, Date_Created, Status FROM Orders")
				cursor.execute(query)
				result = dict()
				for(ID, date, status) in cursor:
					result['dateCreated'] = date.strftime("%c")
					result['status'] = status

				if(len(result)==0):
					result["error"] = "No order with given ID"
					return json.dumps(result, encoding='utf-8')

				if outputformat == 'html':
					return env.get_template('order-tmpl.html').render(info=result)
				elif outputformat == 'json':
					return json.dumps(result, encoding='utf-8')
		else:
			return self.item.index(ID, outputformat)

	@cherrypy.expose
	def index(self, item="", outputformat = 'html'):
		if(cherrypy.request.method=="GET"):
			cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
			cursor = cnx.cursor()

			query = ("SELECT Order_ID, Date_Created, Status FROM Orders")
			cursor.execute(query)
			result = dict()
			for(ID, date, status) in cursor:
				i = dict()
				i['dateCreated'] = date.strftime("%c")
				i['status'] = status
				result[ID] = i

			if outputformat == 'html':
				return env.get_template('orders-tmpl.html').render(info=result)
			elif outputformat == 'json':
				return json.dumps(result, encoding='utf-8')
		elif(cherrypy.request.method=="PUT"):
			if(item!=""):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("SELECT Order_ID, Status FROM Orders WHERE User_ID = %(user)s AND Status = 0")
				cursor.execute(query, { "user": USER })
				orderID = ""
				for(ID, status) in cursor:
					orderID = ID

				if orderID=="": #no order found, create one
					query = ("INSERT INTO Orders (User_ID, Date_Created) VALUES (%(user)s, NOW())")
					cursor.execute(query, { "user": USER})
					orderID = cursor.lastrowid;

				query = ("INSERT INTO Order_Items (Order_ID, Menu_Content_ID) VALUES (%(order)s, %(item)s) on duplicate key update Quantity=Quantity+1")
				cursor.execute(query, { "order": orderID, "item": item })

				cnx.commit()
				cnx.close()
				return json.dumps({ "success": True }, encoding='utf-8')
			else:
				cherrypy.response.headers["Status"] = "400"
				result["error"] = "No item number given"
				return json.dumps(result, encoding='utf-8')
		elif(cherrypy.request.method=="DELETE"):
			if(item!=""):
				cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
				cursor = cnx.cursor()

				query = ("SELECT Order_ID, Date_Created FROM Orders WHERE User_ID = %(user)s AND Status = 0")
				cursor.execute(query, { "user": USER })
				orderID = ""
				for(ID, date) in cursor:
					orderID = ID

				if orderID=="": #no order found, create one
					return "Failure. No order in progress"

				query = ("DELETE FROM Order_Items WHERE Order_ID = %(order)s AND Menu_Content_ID = %(item)s)")
				cursor.execute(query, { "order": orderID, "item": item })

				cnx.commit()
				cnx.close()
				return json.dumps({ "success": True }, encoding='utf-8')
			elif(item==""):
				cherrypy.response.headers["Status"] = "400"
				result["error"] = "No item number given"
				return json.dumps(result, encoding='utf-8')

	@cherrypy.expose
	def checkout(self):
		cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
		cursor = cnx.cursor()

		query = ("UPDATE Orders SET Status=1 WHERE User_ID = %(ID)s;")
		cursor.execute(query, { "ID": USER })
		cnx.commit()
		cnx.close()
		return json.dumps({ "success": True }, encoding='utf-8')