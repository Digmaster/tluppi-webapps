import cherrypy
import sys
import os
sys.path.append('/var/www/wsgi/')
import mysql.connector
from collections import OrderedDict
import mysql.connector
import json
from decimal import *
import random
from Resturant import *
from Order import *
from User import *
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class RootIndex(object):
	def __init__(self):

		self.restaurants = Restaurant()
		self.users = User()
		#self.orders = Order()

	def GET(self):
		cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME, use_unicode=1)
		cursor = cnx.cursor()

		query = ("SELECT Restaurant_ID, name, locality FROM Restaurants")
		cursor.execute(query)

		info = dict()
		for (ID, name, locality) in cursor:
			info[ID] = dict()
			info[ID]["name"] = name
			info[ID]["locality"] = locality

		return env.get_template('restaurants-tmpl.html').render(info=info)